//Vasilii Iurev 2032356
package LinearAlgebra;

public class Vector3dTestManual {
    public static void main(String[] args) {
        Vector3d v = new Vector3d(1, 2, 3.3);
        Vector3d v1 = new Vector3d(1, 6, 10);
        System.out.println(v.getX());
        System.out.println(v.magnitude());
        System.out.println(v.dotProduct(v1));
        System.out.println(v.add(v1).getX());
        System.out.println(v.add(v1).getY());
        System.out.println(v.add(v1).getZ());
    }
}
