//Vasilii Iurev 2032356
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    @Test
    public void testGetMethods() {
        Vector3d v = new Vector3d(1, 2, 3.3);
        assertEquals(1, v.getX());
        assertEquals(2, v.getY());
        assertEquals(3.3, v.getZ());
    }
    @Test
    public void testMagnitudeMethod(){
        Vector3d v = new Vector3d(1, 2, 3.3);
        assertEquals(3.9862262855989496, v.magnitude());
    }
    @Test
    public void testDotProductMethod(){
        Vector3d v = new Vector3d(1, 2, 3.3);
        Vector3d v1 = new Vector3d(1, 6, 10);
        assertEquals(46.0, v.dotProduct(v1));
    }
    @Test
    public void testAddMethod(){
        Vector3d v = new Vector3d(1, 2, 3.3);
        Vector3d v1 = new Vector3d(1, 6, 10);
        assertEquals(2.0, v.add(v1).getX());
        assertEquals(8.0, v.add(v1).getY());
        assertEquals(13.3, v.add(v1).getZ());
    }
}
